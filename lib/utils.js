//==============================================================================
const Utils = {
  //----------------------------------------------------------------------------
  MsgType: Object.freeze({DONE:0,ERROR:1,WARNING:2,PROCESSING:3}),
  //----------------------------------------------------------------------------
  throw: (node,error)=>{
    Utils.status(node,error.toString(),{type:Utils.MsgType.ERROR,sticky:true});
    if (error instanceof Error) throw error;
    else throw new Error(error);
  },
  //----------------------------------------------------------------------------
  status: (node,msg,opts)=>{
    if (node.statusTimer) clearTimeout(node.statusTimer);
    opts = opts || {};
    opts.type  = opts.type || Utils.MsgType.DONE;
    opts.sticky = opts.sticky || false;
    opts.time = opts.time || 3;
    switch (opts.type) {
      case Utils.MsgType.ERROR: opts.fill = "red"; opts.shape = "dot"; break;
      case Utils.MsgType.WARNING: opts.fill = "yellow"; opts.shape = "ring"; break;
      case Utils.MsgType.PROCESSING: opts.fill = "green"; opts.shape = "ring"; break;
      default: opts.fill = "green"; opts.shape = "dot"; break;
    }
    node.status({fill:opts.fill,shape:opts.shape,text:msg});
    if (!opts.sticky) {
      node.statusTimer = setTimeout(()=>{node.status({});},opts.time*1000);
    }
  }
}
//==============================================================================
module.exports=Utils;
