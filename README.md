# nodered-contrib-system-utils
A collection of node-red nodes for common system tasks.

### Keystore Nodes
Manages credentials in the operating systems key storage

On MacOS the passwords are managed by the Keychain, on Linux they are managed by the Secret Service API/libsecret, on Windows they are managed by the Credential Vault.

The keystore nodes allow you to get / set and delete credentials from the OS'es native credential store.

### Iterate Node
Allows serial iteration over arrays.

Looping through an array one by one can be a hassle because of the way Node-Red is built. If you send a message for each item in the array, the output will be triggered for each item regardless wether the processing of the previous item has completed.

The iterate node allows serial execution of array items, triggering the next item only after the previous one has been executed.

![Example](https://bytebucket.org/jeromebei/node-red-contrib-system-utilities/raw/2388f2fde7d99b10812c651bba97627cdc9d4cd0/images/iterate.png)

### Config Nodes
The config store and config load nodes allow you to easily save and load an object to and from the file system. The object to persist / load can be a property of **msg**, **flow** or **global** scope.

You can specify the config file with an absolute path, or just the file name. The configuration will be stored / read in Json format. 

The config store will automatically create the storage folder(s) if inexistent. If no folder is specified, the configuration file will be stored in the user's node-red folder.

# Installation
[![NPM](https://nodei.co/npm/nodered-contrib-system-utils.png?downloads=true)](https://nodei.co/npm/nodered-contrib-system-utils/)

You can install the nodes using node-red's "Manage palette" in the side bar.

Or run the following command in the root directory of your Node-RED installation

    npm install nodered-contrib-system-utils --save

# Dependencies
The nodes are tested with `Node.js v12.18.2` and `Node-RED v1.2.9`.

 - [keytar 7.3.0](https://www.npmjs.com/package/keytar)

# License
[<img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GPLv3" >](http://www.gnu.org/licenses/gpl-3.0.html)

nodered-contrib-system-utils is a free software project licensed under the GNU General Public License v3.0 (GPLv3) by Jérôme Bei.
