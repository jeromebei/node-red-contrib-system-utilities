const Utils = require('../lib/utils.js');
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  function JB_SYSUTILS_Iterate(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    var array = null;
    var idx=0;
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      //------------------------------------------------------------------------
      if (array==null) {
        array=getArray();
        idx=0;
        if (array.length>0) return next();
        else return done();
      } else {
        if (array.length>idx) return next();
        else return done();
      }
      //------------------------------------------------------------------------
      function next() {
        Utils.status(node,"looping "+(idx+1)+" of "+array.length,{type:Utils.MsgType.PROCESSING});
        setArray();
        node.send([msg,null]);
        idx++;
      }
      //------------------------------------------------------------------------
      function done() {
        Utils.status(node,"done");
        setDone();
        node.send([null,msg]);
        idx=0;
        array=null;
      }
      //------------------------------------------------------------------------
      function setDone() {
        switch (config.property_out_type) {
          case "msg": msg[config.property_out]=array; break;
          case "flow": node.context().flow.set(config.property_out,array); break;
          case "global": node.context().global.set(config.property_out,array); break;
        }
      }
      //------------------------------------------------------------------------
      function setArray() {
        switch (config.property_out_type) {
          case "msg": msg[config.property_out]=array[idx]; break;
          case "flow": node.context().flow.set(config.property_out,array[idx]); break;
          case "global": node.context().global.set(config.property_out,array[idx]); break;
        }
      }
      //------------------------------------------------------------------------
      function getArray() {
        switch (config.property_in_type) {
          case "msg": return msg[config.property_in]; break;
          case "flow": return node.context().flow.get(config.property_in); break;
          case "global": return node.context().global.get(config.property_in); break;
          default: return Utils.throw(node,"input array not present"); break;
        }
      }
      //------------------------------------------------------------------------
    });
  }
  //============================================================================
  RED.nodes.registerType("jb.utils.iterate",JB_SYSUTILS_Iterate);
  //============================================================================
}
//==============================================================================
