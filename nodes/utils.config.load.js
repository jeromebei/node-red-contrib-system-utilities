const Utils = require('../lib/utils.js');
const fs = require("fs");
const path = require("path");
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  function JB_SYSUTILS_Config_Load(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      //------------------------------------------------------------------------
      if (path.dirname(config.file)==".") config.file=path.join(RED.settings.userDir,config.file);
      if (!fs.existsSync(config.file)) Utils.throw(node,"config file does not exist");
      try {
        var object = JSON.parse(fs.readFileSync(config.file,"utf8"));
        if (object.config==null) Utils.throw(node,"config does not exist");
        switch (config.property_type) {
          case "msg":    msg[config.property] = object.config ; break;
          case "flow":   node.context().flow.set(config.property,object.config); break;
          case "global": node.context().global.set(config.property,object.config); break;
        }
        Utils.status(node,"done");
        return node.send(msg);
      } catch (e) {
        Utils.throw(node,"could not load: "+e.message);
      }
      //------------------------------------------------------------------------
    });
  }
  //============================================================================
  RED.nodes.registerType("jb.utils.config.load",JB_SYSUTILS_Config_Load);
  //============================================================================
}
//==============================================================================
