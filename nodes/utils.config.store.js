const Utils = require('../lib/utils.js');
const fs = require("fs");
const path = require("path");
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  function JB_SYSUTILS_Config_Store(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      //------------------------------------------------------------------------
      var object={config:null};
      switch (config.property_type) {
        case "msg":    object.config = msg[config.property]; break;
        case "flow":   object.config = node.context().flow.get(config.property); break;
        case "global": object.config = node.context().global.get(config.property); break;
      }
      if (object.config==null) Utils.throw(node,"property does not exist");
      try {
        if (path.dirname(config.file)==".") config.file=path.join(RED.settings.userDir,config.file);
        fs.mkdirSync(path.dirname(config.file),{recursive:true});
        fs.writeFileSync(config.file,JSON.stringify(object),"utf8");
        Utils.status(node,"done");
        return node.send(msg);
      } catch (e) {
        Utils.throw(node,"could not store: "+e.message);
      }
      //------------------------------------------------------------------------
    });
  }
  //============================================================================
  RED.nodes.registerType("jb.utils.config.store",JB_SYSUTILS_Config_Store);
  //============================================================================
}
//==============================================================================
