const keytar = require('keytar');
const Utils = require('../lib/utils.js');
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  function JB_SYSUTILS_Keystore_Delete(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      //------------------------------------------------------------------------
      msg.service = msg.service || config.service;
      msg.account = msg.account || config.account;
      //------------------------------------------------------------------------
      keytar.deletePassword(msg.service, msg.account).then((result)=>{
          if (!result) return Utils.throw(node,"service or account not found");
          Utils.status(node,"done");
          node.send([msg,null]);
      }).catch((error)=>{
        return Utils.throw(node,error);
      });
      //------------------------------------------------------------------------
    });
  }
  //============================================================================
  RED.nodes.registerType("jb.keystore.delete",JB_SYSUTILS_Keystore_Delete);
  //============================================================================
}
//==============================================================================
