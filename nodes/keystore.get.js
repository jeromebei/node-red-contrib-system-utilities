const Utils = require('../lib/utils.js');
const keytar = require('keytar');
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  function JB_SYSUTILS_Keystore_Get(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      //------------------------------------------------------------------------
      msg.service = msg.service || config.service;
      msg.account = msg.account || config.account;
      //------------------------------------------------------------------------
      keytar.getPassword(msg.service, msg.account).then((password)=>{
          msg.password=password;
          Utils.status(node,"done");
          node.send([msg,null]);
      }).catch((error)=>{
        return Utils.throw(node,error);
      });
      //------------------------------------------------------------------------
    });
  }
  //============================================================================
  RED.nodes.registerType("jb.keystore.get",JB_SYSUTILS_Keystore_Get);
  //============================================================================
}
//==============================================================================
