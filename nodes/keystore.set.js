const Utils = require('../lib/utils.js');
const keytar = require('keytar');
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  function JB_SYSUTILS_Keystore_Set(config) {
    RED.nodes.createNode(this,config);
    var node = this;
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      //------------------------------------------------------------------------
      msg.service = msg.service || config.service;
      msg.account = msg.account || config.account;
      if (!msg.password) return Utils.throw(node,"password not set");

      //------------------------------------------------------------------------
      keytar.setPassword(msg.service, msg.account, msg.password).then(()=>{
          delete msg.password;
          Utils.status(node,"done");
          node.send([msg,null]);
      }).catch((error)=>{
        return Utils.throw(node,error);
      });
      //------------------------------------------------------------------------
    });
  }
  //============================================================================
  RED.nodes.registerType("jb.keystore.set",JB_SYSUTILS_Keystore_Set);
  //============================================================================
}
//==============================================================================
